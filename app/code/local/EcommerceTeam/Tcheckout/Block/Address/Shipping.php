<?php

class EcommerceTeam_Tcheckout_Block_Address_Shipping extends Mage_Checkout_Block_Onepage_Abstract
{
	
	protected $_address;
	
    protected function _construct()
    {
        $this->getCheckout()->setStepData('shipping', array(
            'label'     => Mage::helper('checkout')->__('Shipping Information'),
            'is_show'   => $this->isShow()
        ));

        parent::_construct();
    }

    public function getMethod()
    {
        return $this->getQuote()->getCheckoutMethod();
    }

    public function getAddress()
    {
    	
    	if (is_null($this->_address)) {
	        if (!$this->isCustomerLoggedIn()) {
	            $this->_address = $this->getQuote()->getShippingAddress();
	        } else {
	        	
	        	if($this->getQuote()->getShippingAddress()){
	        		$this->_address = $this->getQuote()->getShippingAddress();
	        	}else{
	            	$this->_address = Mage::getModel('sales/quote_address');
	            }
	        	
	        }
        }
        
        return $this->_address;
    }

    /**
     * Retrieve is allow and show block
     *
     * @return bool
     */
    public function isShow()
    {
        return !$this->getQuote()->isVirtual();
    }
}
