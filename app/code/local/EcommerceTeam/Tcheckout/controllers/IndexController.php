<?php

class EcommerceTeam_Tcheckout_IndexController extends Mage_Checkout_Controller_Action {

    protected $_onepage;
    protected $_session;
    protected $_checkout;
    protected $_helper;

    public function getHelper() {

        if (is_null($this->_helper)) {
            $this->_helper = Mage::helper('ecommerceteam_tcheckout');
        }
        return $this->_helper;
    }

    public function getOnepage() {
        if (empty($this->_onepage)) {
            $this->_onepage = Mage::getSingleton('checkout/type_onepage');
        }
        return $this->_onepage;
    }

    public function getCustomerSession() {
        if (empty($this->_session)) {
            $this->_session = Mage::getSingleton('customer/session');
        }
        return $this->_session;
    }

    public function getCheckout() {
        if (empty($this->_checkout)) {
            $this->_checkout = Mage::getSingleton('checkout/session');
        }
        return $this->_checkout;
    }

    protected function _validateQuote() {
        $quote = $this->getOnepage()->getQuote();

        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return false;
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message');
            Mage::getSingleton('checkout/session')->addError($error);
            $this->_redirect('checkout/cart');
            return false;
        }

        return true;
    }

    public function initCheckout() {
        $checkout = $this->getCheckout();
        $customerSession = $this->getCustomerSession();

        $quote = $this->getOnepage()->getQuote();

        /**
         * Reset multishipping flag before any manipulations with quote address
         * addAddress method for quote object related on this flag
         */
        if ($quote->getIsMultiShipping()) {
            $quote->setIsMultiShipping(false);
            $quote->save();
        }

        /*
         * want to laod the correct customer information by assiging to address
         * instead of just loading from sales/quote_address
         */
        $customer = $customerSession->getCustomer();
        if ($customer && !$customerSession->getIsAssignedQuote()) {
            $quote->assignCustomer($customer);
            $customerSession->setIsAssignedQuote(true);
        }
        return $this;
    }

    public function indexAction() {

        $helper = Mage::helper('ecommerceteam_tcheckout');

        Mage::register('tcheckout_current_step', 'address');


        if (!$this->_validateQuote()) {
            return;
        }

        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure' => true)));

        $this->initCheckout();

        if ($this->getRequest()->getParam('is_ajax')) {

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                        'error' => false,
                        'step' => 'address',
                        'content' => $this->_getAddressHtml(),
                        'progress' => $this->_getProgressHtml()
            )));
        } else {

            $this->loadLayout();


            //$this->getLayout()->getUpdate()->removeHandle('checkout_onepage_index');
            //$this->getLayout()->getUpdate()->addHandle('ecommerceteam_tcheckout_index_index');


            $this->_initLayoutMessages('customer/session');
            $this->_initLayoutMessages('checkout/session');

            $this->renderLayout();
        }
    }

    public function addActionLayoutHandles() {
        $update = $this->getLayout()->getUpdate();

        // load store handle
        $update->addHandle('STORE_' . Mage::app()->getStore()->getCode());

        // load theme handle
        $package = Mage::getSingleton('core/design_package');
        $update->addHandle('THEME_' . $package->getArea() . '_' . $package->getPackageName() . '_' . $package->getTheme('layout'));

        // load action handle

        $full_action_name = strtolower($this->getFullActionName());
        if ($full_action_name == 'checkout_onepage_index') {
            $update->addHandle('ecommerceteam_tcheckout_index_index');
        } else {
            $update->addHandle(strtolower($full_action_name));
        }

        return $this;
    }

    public function methodsAction() {


        $helper = Mage::helper('ecommerceteam_tcheckout');
        $checkout = $this->getCheckout();

        Mage::register('tcheckout_current_step', 'methods');

        if (!$this->_validateQuote()) {
            return;
        }

        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);


        if ($this->getRequest()->getParam('is_ajax')) {

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
                        'error' => false,
                        'step' => 'methods',
                        'back_url' => Mage::app()->getStore()->getUrl('*/*/index'),
                        'content' => $this->_getMethodsHtml(),
                        'progress' => $this->_getProgressHtml()
            )));
        } else {



            $this->loadLayout();

            $this->_initLayoutMessages('customer/session');
            $this->_initLayoutMessages('checkout/session');

            $this->renderLayout();
        }
    }

    public function reviewAction() {

        $helper = Mage::helper('ecommerceteam_tcheckout');

        if (!$this->_validateQuote()) {
            return $this->_redirect('checkout/cart');
        }

        Mage::register('tcheckout_current_step', 'review');

        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);

        $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');

        $this->renderLayout();
    }

    public function saveAddressAction() {

        $checkout = $this->getCheckout();
        $result = array('error' => false, 'message' => array());
        $quote = $this->getOnepage()->getQuote();
        $allowed_guest_checkout = Mage::helper('checkout')->isAllowedGuestCheckout($quote);


        if ($this->getRequest()->isPost()) {

            if ($this->getCustomerSession()->isLoggedIn()) {

                $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_LOGIN_IN);
            } elseif ($this->getRequest()->getParam('create_account') || !$allowed_guest_checkout) {

                $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_REGISTER);
            } else {

                $quote->setCheckoutMethod(Mage_Sales_Model_Quote::CHECKOUT_METHOD_GUEST);
            }
    		if (!$this->getRequest()->isPost()) {
                    echo '!isPost'; 
                    return;
                }

                $websiteId = Mage::app()->getWebsite()->getId();
                $data = $this->getRequest()->getPost('billing', array());
                $email = $data['email'];

                $customer = Mage::getModel('customer/customer');

                if ($websiteId) {
                    $customer->setWebsiteId($websiteId);
                    }

                $customer->loadByEmail($email);
                if ($customer->getId()) {
                    $result['error'] = true;
                    $messages = array();
                    $url = Mage::getUrl('customer/account/login/');
                    $messages[] = $this->__('There is already an account with this email address.');
                    $result['message_email'] = array_merge($result['message'], $messages);
                }

            $billing = $this->getRequest()->getPost('billing');

            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            $billing_result = $this->getOnepage()->saveBilling($billing, $customerAddressId);

            
            if (isset($billing_result['error']) && intval($billing_result['error'])) {

                $result['error'] = true;
                $messages = array();
                foreach ((array) $billing_result['message'] as $message) {
                    $messages[] = $this->__('Billing Address Error') . ': ' . $message;
                }

                $result['message'] = array_merge($result['message'], $messages);
            }



            if (!$quote->isVirtual()) {

                if (!isset($billing['use_for_shipping']) || !intval($billing['use_for_shipping'])) {

                    Mage::getSingleton('checkout/session')->setShippingSameAsBilling(false);

                    $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);

                    $shipping = $this->getRequest()->getPost('shipping');

                    $shiping_result = $this->getOnepage()->saveShipping($shipping, $customerAddressId);

                    if (isset($shiping_result['error']) && intval($shiping_result['error'])) {

                        $result['error'] = true;
                        $messages = array();

                        foreach ((array) $shiping_result['message'] as $message) {
                            $messages[] = $this->__('Shipping Address Error') . ': ' . $message;
                        }
                        $result['message'] = array_merge($result['message'], $messages);
                    }
                } else {
                    Mage::getSingleton('checkout/session')->setShippingSameAsBilling(true);
                }
            }

            if ($this->getRequest()->getParam('subscribe', false)) {
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                } else {
                    $email = $this->getOnepage()->getQuote()->getBillingAddress()->getEmail();
                }
                /** @var $newsletterModel Mage_Newsletter_Model_Subscriber */
                $newsletterModel = Mage::getModel('newsletter/subscriber');
                $newsletterModel->subscribe($email);
            }
        } else {

            $result = array('error' => true);
        }

        if ($this->getRequest()->getParam('is_ajax')) {

            if (!(isset($result['error']) && $result['error'])) {

                if ($quote->isVirtual()) {
                    $result['content'] = $this->_getReviewHtml();
                } else {
                    $result['content'] = $this->_getMethodsHtml();
                }
                $result['back_url'] = Mage::app()->getStore()->getUrl('*/*/index');

                if ($quote->isVirtual()) {
                    Mage::register('tcheckout_current_step', 'review');
                    $result['step'] = 'review';
                } else {
                    Mage::register('tcheckout_current_step', 'methods');
                    $result['step'] = 'methods';
                }

                $result['progress'] = $this->_getProgressHtml();
            }



            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {

            if (isset($result['error']) && $result['error']) {

                foreach ($result['message'] as $msg) {

                    Mage::getSingleton('checkout/session')->addError($msg);
                }

                return $this->_redirect('*/*/index');
            } else {
                if ($quote->isVirtual()) {

                    return $this->_redirect('*/*/review');
                } else {

                    return $this->_redirect('*/*/methods');
                }
            }
        }
    }

    public function loginAction() {

        $result = new stdClass();
        $result->error = true;

        $username = $this->getRequest()->getPost('username');
        $password = $this->getRequest()->getPost('password');

        if ($username && $password) {

            try {

                Mage::getSingleton('customer/session')->login($username, $password);
                $result->error = false;
            } catch (Mage_Core_Exception $e) {

                switch ($e->getCode()) {
                    case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                        $message = $this->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', Mage::helper('customer')->getEmailConfirmationUrl($login['username']));
                        break;
                    default:
                        $message = $e->getMessage();
                        break;
                }

                $result->error = true;
                $result->message = $message;
            } catch (Exception $e) {
                $result->error = true;
                $result->message = $e->getMessage();
            }
        } else {
            $result->error = true;
            $result->message = $this->__('Login and password are required');
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function saveMethodsAction() {

        $checkout = $this->getCheckout();
        $result = array('error' => false, 'message' => array());
        $quote = $this->getOnepage()->getQuote();


        if ($this->getRequest()->isPost()) {

            $shipping_method = $this->getRequest()->getPost('shipping_method');

            if (!$shipping_method) {

                $result = array('error' => true, 'message' => array(Mage::helper('ecommerceteam_tcheckout')->__('Please specify shipping method.')));
            } else {

                $result = $this->getOnepage()->saveShippingMethod($shipping_method);
                if (!$result) {
                    Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array('request' => $this->getRequest(), 'quote' => $this->getOnepage()->getQuote()));
                }
                $this->getOnepage()->getQuote()->save();
            }
        } else {

            $result = array('error' => true);
        }



        if ($this->getRequest()->getParam('is_ajax')) {

            if (!(isset($result['error']) && $result['error'])) {


                if ($quote->isVirtual()) {

                    $result['back_url'] = Mage::app()->getStore()->getUrl('*/*/index');
                } else {

                    $result['back_url'] = Mage::app()->getStore()->getUrl('*/*/methods');
                }
                Mage::register('tcheckout_current_step', 'review');

                $result['step'] = 'review';


                $result['content'] = $this->_getReviewHtml();
                $result['progress'] = $this->_getProgressHtml();
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {

            if (isset($result['error']) && $result['error']) {

                foreach ($result['message'] as $msg) {

                    Mage::getSingleton('checkout/session')->addError($msg);
                }

                return $this->_redirect('*/*/methods');
            } else {
                return $this->_redirect('*/*/review');
            }
        }
    }

    public function saveOrderAction() {

        $checkout = $this->getCheckout();
        $result = array('error' => false, 'message' => array());

        try {

            if ($requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds()) {
                $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
                    $this->getCustomerSession()->addError($this->__('Please agree to all Terms and Conditions before placing the order.'));
                    return $this->_redirect('*/*/review');
                }
            }

            if ($data = $this->getRequest()->getPost('payment', false)) {
                $this->getOnepage()->savePayment($data);
                $this->getOnepage()->getQuote()->getPayment()->importData($data);

                if ($redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl()) {

                    $this->getOnepage()->getQuote()->save();

                    return $this->_redirectUrl($redirectUrl);
                }
            }
            $this->getOnepage()->getQuote()->collectTotals();
            $this->getOnepage()->saveOrder();

            $this->getCustomerSession()->setIsAssignedQuote(false);

            if ($redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl()) {
                return $this->_redirectUrl($redirectUrl);
            }

            $this->_redirect('checkout/onepage/success');
        } catch (Mage_Core_Exception $e) {

            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $this->getCustomerSession()->addError($e->getMessage());
            $this->_redirect('*/*/review');
        } catch (Exception $e) {

            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $this->getCustomerSession()->addError($e);
            $this->getCustomerSession()->addError($this->__('There was an error processing your order. Please contact us or try again later.'));
            $this->_redirect('*/*/review');
        }

        $this->getOnepage()->getQuote()->save();
    }

    protected function _getAddressHtml() {
        $layout = Mage::getModel('core/layout');
        $layout->getUpdate()->load('ecommerceteam_tcheckout_ajax_address');
        $layout->generateXml();
        $layout->generateBlocks();
        return $layout->getOutput();
    }

    protected function _getMethodsHtml() {
        $layout = Mage::getModel('core/layout');
        $layout->getUpdate()->load('ecommerceteam_tcheckout_ajax_methods');
        $layout->generateXml();
        $layout->generateBlocks();
        return $layout->getOutput();
    }

    protected function _getReviewHtml() {
        $layout = Mage::getModel('core/layout');
        $layout->getUpdate()->load('ecommerceteam_tcheckout_ajax_review');
        $layout->generateXml();
        $layout->generateBlocks();
        return $layout->getOutput();
    }

    protected function _getProgressHtml() {
        $layout = Mage::getModel('core/layout');
        $layout->getUpdate()->load('ecommerceteam_tcheckout_ajax_progress');
        $layout->generateXml();
        $layout->generateBlocks();
        return $layout->getOutput();
    }

}
