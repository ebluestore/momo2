<?php

class NextBits_HidePrices_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function hideButtonNLinks()
	{
		$status = Mage::getStoreConfig('catalog/hideprices/hide_addtocart_for_guests');
		if($status == 1){
			if(Mage::helper('customer')->isLoggedIn()){
				return 0;
			}
		}
		return $status;
	}
    /**
     * Is price hiding for guests enabled?
     *
     * Checks the config in admin to see if price hiding for guests is enabled.
     * If enabled all product prices will be hidden from guests
     *
    */
    public function hidePricesForGuestsEnabled()
    {  
        $status = Mage::getStoreConfig('catalog/hideprices/hide_prices_for_guests');
		if($status == 1){
			if(Mage::helper('customer')->isLoggedIn()){
				return 0;
			}
		}
        return $status;
    }

    /**
     * Get the message shown to guests instead of price
     *
     * Checks the configuration in admin for the message specified to show guest
     * users instead of price. If empty, will set a default message.
     *
    */
    public function getGuestPricingMessage()
    {
        $message = Mage::getStoreConfig('catalog/hideprices/message_to_display_to_guests');

        if (empty($message) || '' == $message) {

            $message = $this->__('You must be logged in to see pricing');
        }

        return $message;
    }

    /**
     * Is call for price enabled?
     *
     * Checks the config in admin to see if "call for price" is enabled. if so,
     * ALL products will show the call for price message rather than a price.
     *
    */
    public function callForPriceEnabled()
    {
        $status = Mage::getStoreConfig('catalog/hideprices/call_for_price_enabled');
	
        return $status;
    }

    /**
     * Get call for price message
     *
     * Checks the admin configuration for the "call for price" message to be
     * displayed instead of the price.
     *
    */
    public function getCallForPriceMessage()
    {
        $message = Mage::getStoreConfig('catalog/hideprices/call_for_price_message_to_display');

        if (empty($message) || '' == $message) {

            $message = $this->__('Call for pricing');
        }

        return $message;
    }

    /**
     * Get add to cart message
     *
     * Checks the admin configuration for the "add to cart" message to be
     * displayed instead of the price.
     *
    */
    public function getAddToCartMessage()
    {
        $message = Mage::getStoreConfig('catalog/hideprices/add_to_cart_message_to_display');

        if (empty($message) || '' == $message) {

            $message = $this->__('Add to cart to see price!');
        }

        return $message;
    }

    /**
     * Get price hiding attributes
     *
     * Checks a product for it's price hiding attributes and returns the status
     * of each attribute in an array
     *
    */
    public function getPriceHidingAttributes($product)
    {
        $product->load($product->getId());

        $attributes                           = array();
        $attributes['call_for_price']         = $product->getResource()
                                                        ->getAttribute('r2b_call_for_price')
                                                        ->getFrontend()
                                                        ->getValue($product);
        $attributes['hide_price_from_guests'] = $product->getResource()
                                                        ->getAttribute('r2b_hide_price_from_guests')
                                                        ->getFrontend()
                                                        ->getValue($product);

        $attributes['add_to_see_price']       = $product->getResource()
                                                        ->getAttribute('r2b_add_to_see_price')
                                                        ->getFrontend()
                                                        ->getValue($product);

        foreach ($attributes as &$attribute) {

            if (!isset($attribute) || empty($attribute) || '' == $attribute
                || 'no' == strtolower($attribute) || 'off' == strtolower($attribute)
                || 'false' == strtolower($attribute) || !$attribute) {

                $attribute = false;

            } else {

                $attribute = true;
            }
        }

        return $attributes;
    }

    /**
     * Hide product from guest?
     *
     * Checks whether we need to hide this product from guests. Will first
     * check if the customer is logged in which would make subsequent checks
     * moot. Then it checks the admin config value to see if we're hiding ALL
     * products and it checks the product attribute hide for guest, and if
     * either of those are true it returns true
     *
    */
    public function hideProductFromGuest($product)
    {
      

      //  $attributes        = $this->getPriceHidingAttributes($product);
        $hidePricesEnabled = $this->hidePricesForGuestsEnabled();

        if ($hidePricesEnabled) {

            return true;

        } else {

            return false;
        }
    }

    /**
     * Add to cart to see price?
     *
     * Checks if the product needs to be added to the cart in order to see price
     *
    */
    public function addToSeePrice($product)
    {
        $attributes        = $this->getPriceHidingAttributes($product);

        if ($attributes['add_to_see_price']) {

            return true;

        } else {

            return false;
        }
    }

    /**
     * Is call for price active for this product?
     *
     * Will check the admin config to see if call for price is active for ALL
     * products and check the specific product attributes to see if it is active
     * for this product, and if either are true it will return true
     *
    */
    public function getCallForPriceActive($product)
    {
        $attributes          = $this->getPriceHidingAttributes($product);
        $callForPriceEnabled = $this->callForPriceEnabled();

        if ($callForPriceEnabled || $attributes['call_for_price']) {

            return true;

        } else {

            return false;
        }
    }

    /**
     * Is the product saleable?
     *
     * Checks to see if a product is saleable, ie. if it can be added to the cart.
     * Will check if call for price is active for the product first, then will
     * check if hide from guests is active and applicable. If either of those checks
     * are positive it will return the saleable status as false and the message set
     * in config. If true then the product is saleable
     *
    */
    public function getIsSaleable($product)
    {
        $isSaleable = array( 'message' => '', 'showprice' => true, 'addtocart' => true );
		
        if ($this->hideProductFromGuest($product)) {
			
            $isSaleable['showprice']    = false;
            $isSaleable['addtocart'] = false;
            $isSaleable['message']   = $this->getGuestPricingMessage();

        } 
        return $isSaleable;
    }
}
