<?php
class NextBits_HidePrices_Model_Observer
{
	public function addNewLayout($observer){
		$layout = $observer->getEvent()->getLayout();
		$update = $layout->getUpdate();
		$hideHelper = Mage::helper('hideprices'); //HidePrices helper
		$isSaleable = $hideHelper->hidePricesForGuestsEnabled();
		if($isSaleable){
			$xml = "<reference name='content'><remove name='product.info.options.wrapper'>
					</remove><remove name='product.info.bundle'></remove>
					<remove name='product.clone_prices'></remove>
					<remove name='bundle.tierprices'></remove>
					<remove name='product.info.qtyincrements'></remove>
					<remove name='product.info.simple'></remove>
					<remove name='product.info.configurable'></remove>
					<remove name='product.info.grouped'></remove>
					<remove name='product.info.virtual'></remove>
					</reference>";
			$update->addUpdate($xml);
		}
		
		$flag = $hideHelper->hideButtonNLinks();
		if ($flag){
			$xml = "<reference name='content'><remove name='product.info.addtocart'>
					</remove><remove name='product.info.addto'></remove></reference>";
			$update->addUpdate($xml);
		}
		
		if($isSaleable && $flag){
			$xml = "<reference name='content'><remove name='product.info.options.wrapper.bottom'></remove></reference>";
			$update->addUpdate($xml);
		}
		return;
	}
}