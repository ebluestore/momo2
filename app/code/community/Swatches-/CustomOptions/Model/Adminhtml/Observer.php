<?php
class Swatches_CustomOptions_Model_Adminhtml_Observer
{
    public function copySwatchFromGlobalGroup(Varien_Event_Observer $observer)
    {
        $productOptionValue = $observer->getData('product_option_value');
        $globalOptionValue  = $observer->getData('global_option_value');
        try {
            Mage::getResourceModel('swatches_customoptions/product_option_image')
                ->copySwatchFromGlobalGroup($globalOptionValue, $productOptionValue);
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}
