<?php

/**
 * HTML select element block with currencies
 *
 * @category   Webinse
 * @package    Webinse_OrderPdf
 * @author     Webinse Team <alena.tsareva@webinse.com>
 */
class Webinse_OrderPdf_Block_Adminhtml_Form_Field_Items extends Mage_Core_Block_Html_Select
{

    /**
     * Retrieve available block
     *
     * @return array
     */
    protected function _getAvailableBlock()
    {
        return array(
            'base_info'        => 'Base Information',
            'account_info'     => 'Account Information',
            'billing_address'  => 'Billing Address',
            'shipping_address' => 'Shipping Address',
            'payment_info'     => 'Payment Information',
            'shipping_handing' => 'Shipping & Handling Information',
            'items_ordered'    => 'Items Ordered',
            'totals'           => 'Order Totals',
        );
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->addOption(0, $this->__('--Select--'));

            foreach ($this->_getAvailableBlock() as $key => $block)
                $this->addOption($key, addslashes($block));
        }
        return parent::_toHtml();
    }

}
