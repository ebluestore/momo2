<?php

/**
 * Backend for serialized array data
 *
 * @category   Webinse
 * @package    Webinse_OrderPdf
 * @author     Webinse Team <alena.tsareva@webinse.com>
 */
class Webinse_OrderPdf_Model_System_Config_Backend_OrderItem extends Mage_Core_Model_Config_Data
{

    /**
     * Process data after load
     */
    protected function _afterLoad()
    {
        $value = Mage::helper('wb_orderpdf/orderItems')->makeArrayFieldValue($this->getValue());
        $this->setValue($value);
    }

    /**
     * Prepare data before save
     */
    protected function _beforeSave()
    {
        $value = Mage::helper('wb_orderpdf/orderItems')->makeStorableArrayFieldValue($this->getValue());
        $this->setValue($value);
    }

}
