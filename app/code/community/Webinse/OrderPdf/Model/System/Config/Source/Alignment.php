<?php
/**
 * Created by PhpStorm.
 * User: evgenia
 * Date: 7/14/14
 * Time: 5:00 PM
 */

class Webinse_OrderPdf_Model_System_Config_Source_Alignment
{
    public function toOptionArray()
    {
        $helper = Mage::helper('wb_orderpdf');
        return array(
            array('value' => 'left', 'label' => $helper->__('Left')),
            array('value' => 'center', 'label' => $helper->__('Center')),
            array('value' => 'right', 'label' => $helper->__('Right')),
        );
    }
} 