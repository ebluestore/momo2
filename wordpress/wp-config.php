<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mokky_blog');

/** MySQL database username */
define('DB_USER', 'mokky_wordpress');

/** MySQL database password */
define('DB_PASSWORD', '4;h2.rNIa6$D');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OK4RTFl-,,NnK,dPVQ$svwi.C-7g%jnP99fOV1u,  zYREM?*KS0ctXzrF+k|u;C');
define('SECURE_AUTH_KEY',  '7hhnIiTExk=&TgHd#.C?BT[+(G9A0%RJ40E:Dio7_#V[N4DjRlJ}>.1M-]gjGW 1');
define('LOGGED_IN_KEY',    '@A51BbG$b CaA*u.=dp5T|[})?eAcC8S9H5B]K~I|q@*]= (8ES}=rhU*ZaQ)^9k');
define('NONCE_KEY',        'a{fEG=m@Hu%~LGp7%KBg?C6z{V#>-j&zEA*zwUkYY}:eBdV,vTC.:D-0%C%IOtD)');
define('AUTH_SALT',        'Pno3!ECvK2uY!7Xq8+Ai{j7yr|PU8bHzM7V{ gquIhh).GoeP{!qxaviE=+RTCje');
define('SECURE_AUTH_SALT', 'ZjAk=|rEY@1Bk>[W/U%.T~DtiLSy8(WA!uT5*BZ[(<-nM@&P[>|g%OdT.d[!h|o_');
define('LOGGED_IN_SALT',   'H)d&Vg-AbY%im-k1TOP#]wz}c|,`RyA)}3y)KS}{m*U|mBO<[51V,j AK|__<m_T');
define('NONCE_SALT',       'H?(3jz;`$K8j-T1IkRv f`y|!yDe); o4Ya|p&YC%Ch+4eRW~>JI93u)!-0lxql2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
